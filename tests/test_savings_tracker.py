""" Simple test module for testing package __init__. """

from savings_tracker import __version__


def test_version():
    """Check that the version number has the expected value."""
    assert __version__ == "0.1.0"
